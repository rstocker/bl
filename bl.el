;;; bl.el --- Control the display backlight          -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Raffael Stocker

;; Author: Raffael Stocker <r.stocker@mnet-mail.de>
;; Maintainer: Raffael Stocker <r.stocker@mnet-mail.de>
;; Created: 09. Apr 2020
;; Version: 0.0
;; Package-Requires: ((emacs "25.1"))
;; Keywords: hardware
;; URL: https://gitlab.com/rstocker/bl.git

;; This file is NOT part of GNU Emacs

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Control the display backlight.

;;; Code:

(require 'exwm-input)
(require 'battery)

;;;; customization
(defgroup backlight nil
  "Control the display backlight."
  :group 'hardware)
(defcustom bl-brightness-default-inc 10
  "Default increment/decrement value for the display brightness."
  :type '(number))
(defcustom bl-program (executable-find "light")
  "Path to the ‘light’ program."
  :type '(string))
(defcustom bl-brightness-bright 70
  "Default level for a bright display backlight."
  :type '(number))
(defcustom bl-brightness-dim 20
  "Default level for a dim display backlight."
  :type '(number))

(defvar bl--power-source
  (alist-get ?L (and (fboundp battery-status-function)
					 (funcall battery-status-function))))
(defvar bl--last-dim-level bl-brightness-dim)
(defvar bl--last-bright-level bl-brightness-bright)
(defvar bl--autodim-timer nil)

;;;###autoload
(defun bl-brightness-set (level)
  "Set brightness to LEVEL."
  (interactive "NBrightness level (%%): ")
  (call-process bl-program
				nil nil nil
				"-S"
				(number-to-string level))
  (bl-brightness-show))

;;;###autoload
(defun bl-brightness-decrement (&optional value)
  "Decrement the display brightness by VALUE."
  (interactive "P")
  (let ((val (if (numberp value)
				 value
			   bl-brightness-default-inc)))
	(call-process bl-program
				  nil nil nil "-U" (number-to-string val))
	(bl-brightness-show)))

;;;###autoload
(defun bl-brightness-increment (&optional value)
  "Increment the display brightness by VALUE."
  (interactive "P")
  (let ((val (if (numberp value)
				 value
			   bl-brightness-default-inc)))
	(call-process bl-program
				  nil nil nil "-A" (number-to-string val))
	(bl-brightness-show)))

(defun bl-brightness--level ()
  "Return the current brightness level."
  (string-to-number (car (process-lines bl-program "-G"))))

(defun bl--autodim ()
  "Auto dim backlight depending on power line status."
  (let ((source (alist-get ?L (and (fboundp battery-status-function)
								   (funcall battery-status-function)))))
	(unless (string= source bl--power-source)
	  (if (or (string= source "AC")
			  (string= source "connected")
			  (string= source "on-line"))
		  (progn
			(setq bl--last-dim-level (bl-brightness--level))
			(bl-brightness-set bl--last-bright-level))
		(setq bl--last-bright-level (bl-brightness--level))
		(bl-brightness-set bl--last-dim-level))
	  (setq bl--power-source source))))

;;;###autoload
(defun bl-start-autodim ()
  "Start automatic dimming depending on power line status."
  (interactive)
  (setq bl--autodim-timer (run-at-time 0 2 #'bl--autodim)))

(defun bl-stop-autodim ()
  "Stop automatic backlight dimming."
  (interactive)
  (cancel-timer bl--autodim-timer))

;;;###autoload
(defun bl-setup ()
  "Setup backlight control."
  (exwm-input-set-key (kbd "<XF86MonBrightnessDown>")
					  'bl-brightness-decrement)
  (exwm-input-set-key (kbd "<XF86MonBrightnessUp>")
					  'bl-brightness-increment)
  (exwm-input-set-key (kbd "C-<XF86MonBrightnessDown>")
					  #'(lambda ()
						  (interactive)
						  (bl-brightness-set bl-brightness-dim)))
  (exwm-input-set-key (kbd "C-<XF86MonBrightnessUp>")
					  #'(lambda ()
						  (interactive)
						  (bl-brightness-set bl-brightness-bright))))

;;;###autoload
(defun bl-brightness-show ()
  "Display the brightness in the echo area."
  (interactive)
  (message "Brightness level: %.0f %%"
		   (bl-brightness--level)))

(provide 'bl)
;;; bl.el ends here
